import bosdyn.client
import bosdyn.client.util
import bosdyn.client.lease

import time

import customSpotFunctions as cSF
from unityInterface import unity_pb2,  unity_interface, unity_service_pb2_grpc  # Used for camera Stream


class Creds:
    IP = '34.86.61.37'
    name = 'ost'
    psk = '4K8d9s5W'


def main():
    creds = Creds
    bosdyn.client.util.setup_logging(False)

    sdk = bosdyn.client.create_standard_sdk('testroutine')
    sdk.register_service_client(unity_interface.UnityInterfaceClient)

    robot = sdk.create_robot(creds.IP)
    robot.authenticate(creds.name, creds.psk)

    unity_interface_client = robot.ensure_client(unity_interface.UnityInterfaceClient.default_service_name)

    backgroundEStop = cSF.AsyncEstop(creds)
    backgroundEStop.start()
    time.sleep(6)
    assert not robot.is_estopped()
    robot.logger.info("Authenticated and EStop open")

    clients = cSF.Clients(robot)
    robot.time_sync.wait_for_sync()
    lease = clients.lease.acquire()
    robot.logger.info("Taken Control over Robot")
    try:
        with bosdyn.client.lease.LeaseKeepAlive(clients.lease):
            cSF.powerUp(robot)

            while 1:
                if int(time.process_time()) % 2 == 0:
                    robot.logger.info("0.2")
                    cSF.standUp(robot, clients, 0, 0.2)
                else:
                    robot.logger.info("0.4")
                    cSF.standUp(robot, clients, 0.4, 0.4)

                req = unity_pb2.GetStreamFramesRequest(tracking_frame=True, top_down_frame=True)
                resp = unity_interface_client.get_stream_frames(req)
                tracking_img = cSF.to_cv_image(resp.tracking_frame)

                #top_down_img.imshow()

            #image = cSF.captureImage(clients)
            #print(cSF.captureConfig(clients))

    finally:
        clients.lease.return_lease(lease)
        print("Ended Program")


if __name__ == "__main__":
    main()