"""For clients to the unity service."""

from bosdyn.client.common import BaseClient

from unityInterface import unity_service_pb2_grpc
from unityInterface import unity_pb2


class UnityInterfaceClient(BaseClient):
    """A client that controls the Unity environment of the simulated robot."""
    default_authority = 'unityinterface.spot.robot'
    default_service_name = 'unity-interface'
    service_type = 'blue.unity.UnityInterfaceService'

    def __init__(self):
        super(UnityInterfaceClient,
              self).__init__(unity_service_pb2_grpc.UnityInterfaceServiceStub)

    def get_scene_state(self, **kwargs):
        """Get scene state from server.

        Returns:
           GetSceneStateResponse
        """
        request = unity_pb2.GetSceneStateRequest()
        return self.call(self._stub.GetServerSceneState, request, None, None, **kwargs)

    def set_scene_state(self, request, **kwargs):
        """Set scene state on server.

        Args:
            request: SetSceneStateRequest.

        Returns:
           SetSceneStateResponse
        """
        return self.call(self._stub.SetServerSceneState, request, None, None, **kwargs)

    def get_available_scenes(self, **kwargs):
        """Get all available scenes.

        Returns:
           GetSceneStateResponse
        """
        request = unity_pb2.GetAvailableScenesRequest()
        return self.call(self._stub.GetAvailableScenes, request, None, None, **kwargs)

    def get_stream_frames(self, request, **kwargs):
        """Get one or more stream frames.

        Returns:
           GetStreamFramesResponse
        """
        return self.call(self._stub.GetStreamFrames, request, None, None, **kwargs)
