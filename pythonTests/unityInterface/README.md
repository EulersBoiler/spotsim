# Unity Interface

This set of files enables a developer to manipulate the environment of a simulated Spot. The scene, spawn point, set of fiducials and world objects can be modified. It also lets clients pull from camera streams in the environment to better visualize the simulation.

Currently three environments are provided with the sim: IndustrialEnv, Mountain, Snow.

Documentation on use is provided in the proto and python files.

## Spawnable Objects
- `Banana`
- `Box`
- `BoxPile`
- `BoxSmall`
- `GoalPosts`
- `Pillar`
- `Ring`
- `SmashBoxes`
- `Wall`
- `fire extinguisher`
