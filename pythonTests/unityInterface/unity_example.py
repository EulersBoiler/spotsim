import argparse
import copy
from PIL import Image
import numpy as np
import unity_service_pb2_grpc, unity_pb2
import random
import sys
from google.protobuf.text_format import Parse

from bosdyn.client.robot_state import RobotStateClient
import bosdyn.client.util

from unity_interface import UnityInterfaceClient



def to_pil_image(raw_image_proto):
    """Stream frame images are ARGB32 encoded. Convert into an image object.
    
    They need to be altered to RGBA32 and rotated 180 degrees.
    """
    img = np.frombuffer(raw_image_proto.data, np.uint8)
    img.shape = (raw_image_proto.rows, raw_image_proto.cols, -1)

    # Shift channels from ARGB to RGBA.
    img = img[...,[1, 2, 3, 0]]

    # Mirror the image.
    img = np.flip(img, [0,1])

    # Parse the image from the numpy array.
    img = Image.fromarray(img, 'RGBA')

    return img


# An example scene state with a couple fiducials and nonstandard spawnpoint.
fiducial_0 = unity_pb2.Fiducial(
    tag_id=304,
    world_T_fiducial=unity_pb2.Pose3D(
        position=unity_pb2.Vec3(
            x=-4.0,
            z=0.4
        ),
        orientation=unity_pb2.Quat(
            w=1.0
        )
    )
)

fiducial_1 = unity_pb2.Fiducial(
    tag_id=504,
    world_T_fiducial=unity_pb2.Pose3D(
        position=unity_pb2.Vec3(
            x=-6.0,
            y=0.5,
            z=0.2
        ),
        orientation=unity_pb2.Quat(
            w=1.0
        )
    )
)

banana = unity_pb2.SpawnableWorldObject(
    name="Banana", # Note that name matches exactly.
    world_T_object=unity_pb2.Pose3D(
        position=unity_pb2.Vec3(
            x=-2.0,
            y=-1.0,
            z=0.6
        ),
        orientation=unity_pb2.Quat(
            w=1.0
        )
    )
)

spawn_footprint = unity_pb2.Pose3D(
    position=unity_pb2.Vec3(
        x=0,
        y=0
    ),
    orientation=unity_pb2.Quat(
        z=1.0
    )
)


def call(config):
    sdk = bosdyn.client.create_standard_sdk('UnitySpotExampleSDK')

    sdk.register_service_client(UnityInterfaceClient)
    
    robot = sdk.create_robot(config.hostname)
    robot.authenticate(config.username, config.password)

    unity_interface_client = robot.ensure_client(UnityInterfaceClient.default_service_name)

    # Read all possible scenes.
    available_scenes = unity_interface_client.get_available_scenes().available_scenes
    print("Available Scenes: ", available_scenes, '\n')

   # Configure the scene state.
    scene_state = unity_pb2.SceneState()

    # Scene name determines what the scene looks like. See get_available_scenes() to see options.
    scene_state.scene_name = "IndustrialEnv"

    # Add fiducial objects to the simulation environment.
    scene_state.fiducials.add().CopyFrom(fiducial_0)
    scene_state.fiducials.add().CopyFrom(fiducial_1)

    # Add spawnable objects to simulation environment.
    scene_state.spawnable_objects.add().CopyFrom(banana)

    # world_T_initial_spawn_footprint determines where the robot is located at the start of the scene.
    scene_state.world_T_initial_spawn_footprint.CopyFrom(spawn_footprint)

    # First, try to set the server's scene state to match the string at the top of this file.
    req = unity_pb2.SetSceneStateRequest(scene_state=scene_state)
    resp = unity_interface_client.set_scene_state(req)

    # Now we read it back from the server to make sure it got saved there.
    resp = unity_interface_client.get_scene_state()
    print(resp)

    # We can retrieve the robot state for information about robot transforms.
    # robot_state_client = robot.ensure_client(RobotStateClient.default_service_name)
    # print(robot_state_client.get_robot_state().kinematic_state)

    # Randomly select a pre-made scene to use.
    # new_scene_name = random.choice(available_scenes)
    # req = unity_pb2.SetSceneStateRequest(scene_state=unity_pb2.SceneState(scene_name=new_scene_name))
    # resp = unity_interface_client.set_scene_state(req)

    # Now we read it back from the server to make sure it got saved there.
    # resp = unity_interface_client.get_scene_state()
    # print(resp)


    # Get frames from stream and show / save them.
    req = unity_pb2.GetStreamFramesRequest(tracking_frame=True, top_down_frame=True)
    resp = unity_interface_client.get_stream_frames(req)
    tracking_img = to_pil_image(resp.tracking_frame)
    top_down_img = to_pil_image(resp.top_down_frame)
    tracking_img.show()
    top_down_img.show()


def main(argv):
    """Command line interface."""
    parser = argparse.ArgumentParser()
    bosdyn.client.util.add_common_arguments(parser)
    options = parser.parse_args(argv)
    call(options)

if __name__ == '__main__':
    if not main(sys.argv[1:]):
        sys.exit(1)
