import bosdyn.client
import bosdyn.client.util
import bosdyn.client.lease
import bosdyn.geometry

from bosdyn.client.image import ImageClient
from bosdyn.client.robot_state import RobotStateClient
from bosdyn.client.robot_command import RobotCommandBuilder, RobotCommandClient, blocking_stand

import numpy as np
import time
import threading
import subprocess


class AsyncEstop(threading.Thread):
    def __init__(self, creds):
        threading.Thread.__init__(self)
        self.creds = creds

    def run(self):
        tstring = 'xterm -e python3 bd_estop.py --username ' + self.creds.name + ' --password ' + self.creds.psk + ' ' + self.creds.IP
        subprocess.call(tstring, cwd='/home/spotnuc/spotsim/pythonTests', shell=True)


class Clients:
    def __init__(self, robot):
        self.command = robot.ensure_client(RobotCommandClient.default_service_name)
        self.lease = robot.ensure_client(bosdyn.client.lease.LeaseClient.default_service_name)
        self.image = robot.ensure_client(ImageClient.default_service_name)
        self.robotState = robot.ensure_client(RobotStateClient.default_service_name)


def powerUp(robot):
    robot.logger.info("Powering on robot... This may take several seconds.")
    robot.power_on(timeout_sec=20)
    assert robot.is_powered_on(), "Robot power on failed."
    robot.logger.info("Robot powered on.")


def standUp(robot, clients, direction, height):
    footprint_R_body = bosdyn.geometry.EulerZXY(yaw=direction, roll=0.0, pitch=0.0, )
    cmd = RobotCommandBuilder.synchro_stand_command(footprint_R_body=footprint_R_body, body_height=height)
    clients.command.robot_command(cmd)
    robot.logger.info("Robot standing.")
    time.sleep(3)


def captureImage(clients):
    sources = clients.image.list_image_sources()
    image_response = clients.image.get_image_from_sources(['frontleft_fisheye_image'])
    image = image_response[0].shot.image

    try:
        from PIL import Image
        import io
    except ImportError:
        logger = bosdyn.client.util.get_logger()
        logger.warning("Missing dependencies. Can't display image.")
        return

    image = Image.open(io.BytesIO(image.data))
    return image


def to_cv_image(raw_image_proto):
    try:
        import cv2 as cv
    except ImportError:
        logger = bosdyn.client.util.get_logger()
        logger.warning("Missing dependencies. Can't display image.")
        return

    img = np.frombuffer(raw_image_proto.data, np.uint8)
    img.shape = (raw_image_proto.rows, raw_image_proto.cols, -1)
    img = img[...,[1, 2, 3, 0]]
    img = np.flip(img, [0,1])
    return img


def captureState(clients):
    request_fn = getattr(clients.robotState, "get_robot_state")
    response = request_fn()
    return response


def captureConfig(clients):
    request_fn = getattr(clients.robotState, "get_hardware_config_with_link_info")
    response = request_fn()
    return response
