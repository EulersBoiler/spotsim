import bosdyn.client
import bosdyn.client.util
import bosdyn.client.lease

import time
import cv2 as cv
import numpy as np

import customSpotFunctions as cSF
from unityInterface import unity_pb2,  unity_interface, unity_service_pb2_grpc  # Used for camera Stream


class Creds:
    IP = '34.86.61.37'
    name = 'ost'
    psk = '4K8d9s5W'


def main():
    creds = Creds
    bosdyn.client.util.setup_logging(False)

    sdk = bosdyn.client.create_standard_sdk('testroutine')
    sdk.register_service_client(unity_interface.UnityInterfaceClient)

    robot = sdk.create_robot(creds.IP)
    robot.authenticate(creds.name, creds.psk)

    unity_interface_client = robot.ensure_client(unity_interface.UnityInterfaceClient.default_service_name)

    backgroundEStop = cSF.AsyncEstop(creds)
    backgroundEStop.start()
    time.sleep(5)
    assert not robot.is_estopped()
    robot.logger.info("Authenticated and EStop open")

    clients = cSF.Clients(robot)
    robot.time_sync.wait_for_sync()
    lease = clients.lease.acquire()
    robot.logger.info("Startup. Taken Control over Robot")
    try:
        with bosdyn.client.lease.LeaseKeepAlive(clients.lease):
            cSF.powerUp(robot)

    finally:
        clients.lease.return_lease(lease)
        robot.logger.info("Ended Startup. Streaming Webcam")

    req = unity_pb2.GetStreamFramesRequest(tracking_frame=True, top_down_frame=True)
    while 1:
        resp = unity_interface_client.get_stream_frames(req)
        tracking_img = cSF.to_cv_image(resp.tracking_frame)
        top_down_img = cSF.to_cv_image(resp.top_down_frame)

        cv.imshow('Tracking / Birdseye', np.hstack((tracking_img, top_down_img)))
        time.sleep(0.1)
        cv.waitKey(33)



if __name__ == "__main__":
    main()