import bosdyn.client
import bosdyn.client.util
import bosdyn.client.lease
import bosdyn.geometry

from bosdyn.client.image import ImageClient
from bosdyn.client.robot_state import RobotStateClient
from bosdyn.client.robot_command import RobotCommandBuilder, RobotCommandClient, blocking_stand

import time
import threading
import subprocess


class AsyncEstop(threading.Thread):
    def __init__(self, creds):
        threading.Thread.__init__(self)
        self.creds = creds

    def run(self):
        tstring = 'xterm -e python bd_estop.py --username ' + self.creds.name + ' --password ' + self.creds.psk + ' ' + self.creds.IP
        subprocess.call(tstring, cwd='/home/spotnuc/spotsim/pythonTests', shell=True)


class Clients:
    def __init__(self, robot):
        self.command = robot.ensure_client(RobotCommandClient.default_service_name)
        self.lease = robot.ensure_client(bosdyn.client.lease.LeaseClient.default_service_name)
        self.image = robot.ensure_client(ImageClient.default_service_name)
        self.robotState = robot.ensure_client(RobotStateClient.default_service_name)


class Creds:
    IP = '34.86.61.37'
    name = 'ost'
    psk = '4K8d9s5W'


def main():
    bosdyn.client.util.setup_logging(0)
    creds = Creds

    sdk = bosdyn.client.create_standard_sdk('testroutine')
    robot = sdk.create_robot(creds.IP)
    robot.logger.info("Authenticating")

    res = robot.authenticate(creds.name, creds.psk)
    assert res == None, "Authentication error"
    backgroundEStop = AsyncEstop(creds)
    backgroundEStop.start()
    time.sleep(5)
    assert not robot.is_estopped(), "No EStop running"
    robot.logger.info("Authenticated and EStop open")
    print("Authenticated and EStop open")


if __name__ == "__main__":
    main()